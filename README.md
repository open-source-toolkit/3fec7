# 雷达信号处理实战：线性调频信号与目标回波仿真（MATLAB）

## 概述

本仓库致力于提供一套详细的雷达信号处理教程及实践案例，聚焦于线性调频(LFM)信号的生成、雷达回波的模拟以及脉冲压缩技术。由作者 huasir 于2023年9月21日在北京开发，旨在辅助工程师和学习者深入理解雷达信号处理的核心概念与MATLAB编程应用。

## 主要功能

此资源包含一个MATLAB脚本，通过设定一系列参数，能够实现：

- **线性调频信号**的高效生成，适用于雷达系统的基础研究。
- **雷达回波模拟**，包括对单一或多个目标的反射回波，有助于理解和测试雷达信号的传播与接收特性。
- **脉冲压缩技术**演示，通过匹配滤波器设计，提升雷达系统的分辨率和检测能力。

## 使用说明

### 输入参数

- **bandWidth**: 信号带宽，默认建议值2.0e6 Hz（即2 MHz）。
- **pulseDuration**: 脉冲持续时间，默认40.0e-6秒（即40微秒）。
- **PRTDuration**: 脉冲重复周期，默认240毫秒。
- **samplingFrequency**: 采样频率，推荐为信号带宽的两倍以上。
- **signalPower**: 信号功率，基准值设为1（可根据需要调整）。
- **targetDistance**: 目标距离，其选择需考虑到脉冲重复周期所决定的最大无模糊距离。
- **plotEnableHigh**: 控制是否显示图形，设置为1开启图形界面，0则关闭。

### 输出结果

- **LFMPulse**: 线性调频信号的时间域表示。
- **targetEchoPRT**: 经过模拟的目标反射回波序列。
- **matchedFilterCoeff**: 用于脉冲压缩的匹配滤波器系数。
- **pulseNumber**: 在特定采样率下的脉冲数，帮助了解数据规模。

## 应用场景

- **教育与研究**：教学中解释LFM信号原理和雷达信号处理技术的理想工具。
- **工程开发**：雷达系统开发者进行算法验证和性能评估的实用代码库。
- **自学提升**：对于希望深入了解雷达信号处理的自学者，提供了直接操作的平台。

## 快速上手

1. **克隆仓库**：将此项目复制到本地环境。
2. **环境准备**：确保安装有MATLAB并配置好环境。
3. **修改参数**：根据实际研究或学习需求调整输入参数。
4. **运行脚本**：在MATLAB中打开主脚本并执行。
5. **分析结果**：观察生成的信号图形，理解不同参数变化对信号特性的影响。

通过实践此 MATLAB 仿真项目，用户不仅能够加深对雷达信号处理理论的理解，还能掌握在真实世界问题中应用这些理论的能力。欢迎贡献代码和反馈，共同促进知识共享和技术进步。